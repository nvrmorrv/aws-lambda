## Integration of Aws Lambda with Java

The app uses Aws SDK to communicate with Aws Lambda (test-sample) and Aws SQS(test-queue).
The lambda function accepts json model containing single field "name" and return json response with greeting message.
There are two type of invocations synchronous and asynchronous. The synchronous invocations return
response immediately. The asynchronous invocations pass it to SQS. 

### Setup
While launching the app provide your amazon credentials by specifying the following system parameters:
	-Daws.accessKeyId
	-Daws.secretKey 

### Usage
Endpoints:
	* async execution http://localhost:8080/lambda/execute?name=Denys&type=ASYNC
	* sync execution http://localhost:8080/lambda/execute?name=Denys&type=SYNC
	* message http://localhost:8080/sqs/messages


