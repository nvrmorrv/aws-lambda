package com.example.lambda.dto;

import lombok.Data;

@Data
public class Person {
    private final String name;
}
