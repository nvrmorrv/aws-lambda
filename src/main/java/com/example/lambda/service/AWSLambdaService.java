package com.example.lambda.service;

import com.amazonaws.auth.SystemPropertiesCredentialsProvider;
import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.amazonaws.services.lambda.model.InvocationType;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.amazonaws.services.lambda.model.LogType;
import com.amazonaws.util.StringUtils;
import com.example.lambda.dto.Person;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class AWSLambdaService {
    private final AWSLambda awsLambda;
    private final String funcName;
    private final ObjectMapper mapper;

    public AWSLambdaService(@Value("${aws.lambda.name}") String funcName,
                            @Value("${aws.region}") String region,
                            ObjectMapper mapper) {
        this.funcName = funcName;
        this.mapper = mapper;
        this.awsLambda = AWSLambdaClientBuilder.standard()
                    .withCredentials(new SystemPropertiesCredentialsProvider())
                    .withRegion(region)
                    .build();
    }

    public void invokeAsync(Person person) throws JsonProcessingException {
        InvokeRequest invokeRequest = new InvokeRequest()
                .withFunctionName(funcName)
                .withInvocationType(InvocationType.Event)
                .withLogType(LogType.Tail)
                .withPayload(mapper.writeValueAsString(person));
        InvokeResult result =  awsLambda.invoke(invokeRequest);
        if(!StringUtils.isNullOrEmpty(result.getFunctionError())) {
            throw new RuntimeException(
                    String.format("Failed to invoke lambda asynchronously, person : %s, error: %s, response : %s",
                            person,
                            result.getFunctionError(),
                            result.getPayload()));
        }
    }

    public Map<String, Object> invokeSync(Person person) throws JsonProcessingException {
        InvokeRequest invokeRequest = new InvokeRequest()
                .withFunctionName(funcName)
                .withInvocationType(InvocationType.RequestResponse)
                .withLogType(LogType.Tail)
                .withPayload(mapper.writeValueAsString(person));
        InvokeResult result =  awsLambda.invoke(invokeRequest);
        if(!StringUtils.isNullOrEmpty(result.getFunctionError())) {
            throw new RuntimeException(
                    String.format("Failed to invoke lambda synchronously, person : %s, error: %s, response : %s",
                            person,
                            result.getFunctionError(),
                            result.getPayload()));
        }
        String json = new String(result.getPayload().array());
        return mapper.readValue(json, new TypeReference<>() {});
    }
}
