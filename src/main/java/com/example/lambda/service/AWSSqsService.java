package com.example.lambda.service;

import com.amazonaws.auth.SystemPropertiesCredentialsProvider;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.Message;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class AWSSqsService {
    private final String queueUrl;
    private final AmazonSQS amazonSQS;
    private final ObjectMapper mapper;

    public AWSSqsService(@Value("${aws.sqs.name}") String name, ObjectMapper mapper) {
        this.amazonSQS = AmazonSQSClientBuilder.standard()
                .withCredentials(new SystemPropertiesCredentialsProvider())
                .build();
        this.queueUrl = amazonSQS.getQueueUrl(name).getQueueUrl();
        this.mapper = mapper;
    }

    public List<Object> retrieve() {
        return amazonSQS.receiveMessage(queueUrl).getMessages()
                .stream()
                .map(m -> new String(m.getBody().getBytes(StandardCharsets.UTF_8)))
                .map(s -> {
                    try {
                        return mapper.readValue(s, new TypeReference<>() {});
                    } catch (JsonProcessingException e) {
                        throw new RuntimeException(e);
                    }
                })
                .collect(Collectors.toList());
    }
}
