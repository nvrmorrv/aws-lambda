package com.example.lambda.controller;

import com.example.lambda.service.AWSSqsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequiredArgsConstructor
public class LambdaMessageController {
    private final AWSSqsService awsSqsService;

    @GetMapping("/sqs/messages")
    public ResponseEntity<?> executeSync() {
        try {
            return ResponseEntity.ok(awsSqsService.retrieve());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(Map.of("error", e.getMessage()));
        }
    }
}
