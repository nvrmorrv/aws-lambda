package com.example.lambda.controller;

import com.example.lambda.dto.Person;
import com.example.lambda.service.AWSLambdaService;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequiredArgsConstructor
public class LambdaController {

    private final AWSLambdaService lambdaService;

    @GetMapping("/lambda/execute")
    public ResponseEntity<?> executeSync(
            @RequestParam("name") String name, 
            @RequestParam("type") String type) {
        try {
            return ExecType.valueOf(type).execute(lambdaService, new Person(name));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(Map.of("error", e.getMessage()));
        }
    }

    private enum ExecType {
        SYNC {
            @Override
            public ResponseEntity<?> execute(AWSLambdaService service, Person person) throws JsonProcessingException {
                return ResponseEntity.ok(service.invokeSync(person));
            }
        },
        ASYNC {
            @Override
            public ResponseEntity<Void> execute(AWSLambdaService service, Person person) throws JsonProcessingException {
                service.invokeAsync(person);
                return ResponseEntity.ok().build();
            }
        };

        public abstract ResponseEntity<?> execute(AWSLambdaService service, Person person) throws JsonProcessingException;
    }
}
